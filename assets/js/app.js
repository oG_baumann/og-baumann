function nav (el) {
	el.addEventListener('click',function(){
		var page = this.getAttribute('data-target');
		document.querySelector('.slide.active').classList.remove('active');
		document.querySelector('#'+page).classList.add('active');
		if(page === 'contact') {
			var form = document.querySelector('#contact_form');
			form.classList.remove('complete');
			form.classList.remove('error');
			Array.prototype.slice.call(form.getElementsByClassName('input')).forEach(function(a){
				a.value = "";
			});
		}
	});
}

function submit (e) {
	e.preventDefault();

	var frm = this;
	var inp = Array.prototype.slice.call(frm.querySelectorAll('#contact_form input[type="text"], #contact_form textarea'));
	var ajx = new XMLHttpRequest();
	var dta = inp.reduce(function(acc, a, i){
		var str = !i ? a.name+"="+encodeURIComponent(a.value) : "&"+a.name+"="+encodeURIComponent(a.value);
		return acc + str; 
	}, '')

	ajx.open('POST', '/', true);
   
	ajx.onreadystatechange = function () {
		if(ajx.readyState === 4 && ajx.status === 200) {
			if(JSON.parse(ajx.responseText).status === 'pass') {
				frm.classList.remove('error');
				frm.classList.add('complete');
				inp.forEach(function(el){
					el.value = '';
				});					
			} else {
				frm.classList.add('error');
			}
		}	
	}

 	ajx.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	ajx.send(dta);
}

function navApps () {
	function script (path) {
		var s = document.createElement('script');
			s.type = "text/javascript";
			s.src  = path;
			return s;
	}
	function style (path) {
		var s = document.createElement('link');
			s.rel = "stylesheet";
			s.href = path;
			return s;		
	} 

	var navs  		 = document.querySelectorAll('[data-dir]');

	var main 		 = document.getElementById('app');
	var sub 		 = document.getElementById('app_data');
	var title		 = document.getElementById('app_title');
	var info		 = document.getElementById('app_info');
	var src		 	 = document.getElementById('app_src');

	var activeApp 	 = null;
	var activeScript = null;
	var activeStyle  = null;

	return function (e) {
		if(!e && (!activeStyle || !activeScript)){

			activeApp    = apps.root;
			title.innerHTML = activeApp.name;
			info.innerHTML  = activeApp.info;
			src.href 		= activeApp.src;			
			activeStyle  = style(activeApp.css);
			activeScript = script(activeApp.app);
			
			document.body.appendChild(activeStyle);
			document.body.appendChild(activeScript);

		} else if (e.type === 'click') {
			
			var node = apps.root;
			var dir  = this.getAttribute('data-dir');

			if((dir === 'l' && activeApp.name === apps.root.name) || (dir === 'r' && !activeApp.next)) {
				return;
			} else {
				while (node) {
					if(dir === 'l' && node.next.name === activeApp.name) {
						activeApp = node;
						navs[1].classList.remove('end');
						if(activeApp.name === apps.root.name) {
							navs[0].classList.add('end');
						}
						break;
					} else if (dir === 'r' && node.name === activeApp.name) {
						activeApp = node.next;
						navs[0].classList.remove('end');
						if(!activeApp.next) {
							navs[1].classList.add('end');
						}
						break;
					} else {
						node = node.next;
					}
				}
				window['og_loader'] = false;
				sub.style.visibility = 'hidden';
				main.style.height = main.clientHeight + 'px';
				main.style.visibility = 'hidden';

				main.innerHTML  = "";
				title.innerHTML = activeApp.name;
				info.innerHTML  = activeApp.info;
				src.href 		= activeApp.src;
				activeScript.parentNode.removeChild(activeScript);
				activeStyle.parentNode.removeChild(activeStyle);
				activeStyle  = style(activeApp.css);
				activeScript = script(activeApp.app);
				document.body.appendChild(activeStyle);
				document.body.appendChild(activeScript);

				setTimeout(function loader () {
					if(window['og_loader'] && window['og_loader'] === 'loaded') {
						sub.style.visibility = '';
						main.style.height = '';
						main.style.visibility = '';						
					} else {
						setTimeout(loader, 100);
					}		
				}, 100);

			}						
		}
	}
}

function init(){

	Array.prototype.slice.call(document.querySelectorAll('[data-target]')).forEach(nav);
	document.querySelector('#contact_form').addEventListener('submit', submit);

	var nav_apps = navApps();
	var ajx = new XMLHttpRequest();

	ajx.open('GET', '/assets/app.json', true);
	ajx.onreadystatechange = function () {
		if(ajx.readyState === 4 && ajx.status === 200) {
			window['apps'] = JSON.parse(ajx.responseText);	
			nav_apps();
		} 
	}

	ajx.send(null);

	Array.prototype.slice.call(document.querySelectorAll('[data-dir]')).forEach(function(a){
		a.addEventListener('click', nav_apps);
	});

}

window.addEventListener('load', init);