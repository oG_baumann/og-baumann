

<?php 

	if($_SERVER['REQUEST_METHOD'] == 'POST') {
		$name  = trim(filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING)) ? $_REQUEST['name'] : false;
		$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL) ? $_REQUEST['email'] : false;
		$phone = trim(filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING)) ? $_REQUEST['phone'] : 'NA';
		$msg   = trim(filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING)) ? $_REQUEST['message'] : false;

		if(!$name || !$email || !$msg) {
			echo json_encode(array('status'=>'fail'));

		} else {

			$to      = 'chris@og-baumann.com';
			$subject = 'Message from ' . $name;
			$message = 'Name: ' . $name . "\n\n" . 
					   'Phone: ' . $phone . "\n\n" . 
					   'Email: ' . $email . "\n\n" .
					   'Message: ' . $msg; 
			$headers = 'From: chris@og-baumann.com' . "\r\n" .
			    	   'X-Mailer: PHP/' . phpversion();

			mail($to, $subject, $message, $headers);

			echo json_encode(array('status'=>'pass'));
		}

		exit();
	} 

?>

<!DOCTYPE html>
<html>
<head>
	<title>oG-Baumann</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Roboto+Slab" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/assets/css/app.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
</head>
	<body>
		<header>
			<nav>
				<ul>
					<li data-target="home"><div>Home</div></li>
					<li data-target="about"><div>About</div></li>
					<li data-target="contact"><div>Contact</div></li>
				</ul>
			</nav>
		</header>
		<article>
			<div class="main">
				<div class="track">
					<div class="slide active" id="home">
						<div class="title-nav">
							<div class="end" data-dir="l"></div>
							<h1 id="app_title"></h1>
							<div data-dir='r'></div>
						</div>
						<p>A new app, every month. <i><b>Always</b></i> vanilla javascript.</p>
						<div id="app"></div>
						<div id="app_data">
							<h3>About</h3>
							<p id="app_info"></p>
						</div>
						<h3><a id="app_src" href="" target="_blank">Get The Code</a></h3>
					</div>
					<div class="slide" id="about">
						<h1>What is this stuff?</h1>
						<p><b>Javascript</b>, minus the fluff.</p>
						<p>Every month (or week, I love to code) I will post a new mini app coded completely in Vanilla JS. Most of these apps are modeled after fancy framework tutorials I find online for React, Angular, etc. Be sure to check out the code for each app, or even for <a href="https://bitbucket.org/oG_baumann/og-baumann/">this site</a>, which was written with only Vanilla JS, HTML, CSS, and some PHP.</p>
						<p><b>Best,</b></p>
						<p>Chris</p>
					</div>
					<div class="slide" id="contact">
						<h1>Hit me up!</h1>
						<form id="contact_form">
							<div>
								<input class="input" type="text" name="name" placeholder="Name" required="">
								<input class="input" type="text" name="email" placeholder="Email" required="">
								<input class="input" type="text" name="phone" placeholder="Phone">
								<textarea  class="input"  name="message" placeholder="Say something..." required=""></textarea>
								<input type="submit" name="submit">
							</div>
							<div>
								Thank you for your submission!
							</div>
						</form>
					</div>
				</div>
			</div>
		</article>
		<footer>
			<div>
				<div>
					<a href="https://www.linkedin.com/in/christopher-baumann-210a1151/"><i class="fab fa-linkedin"></i></a>
					<a href="https://bitbucket.org/oG_baumann/"><i class="fab fa-bitbucket"></i></a>
				</div>
				<div>
					<?=date("F j, Y"); ?>
				</div>
			</div>
		</footer>
		<script type="text/javascript" src="/assets/js/app.js"></script>
	</body>
</html>

